# Fase de construcción
FROM maven:3.8.3-openjdk-17-slim AS build

WORKDIR /app

# Copia los archivos fuente al contenedor
COPY src /app/src
COPY pom.xml /app

# Construye la aplicación
RUN mvn clean package -DskipTests

# Fase de ejecución
FROM openjdk:17-jdk-slim

# Copia el JAR construido a la imagen de ejecución
COPY --from=build /app/target/tipo-cambio-0.0.1-SNAPSHOT.jar /app.jar

ARG JAVA_OPTS=""

EXPOSE 8080

ENTRYPOINT ["sh", "-c", "java $JAVA_OPTS -jar /app.jar"]
