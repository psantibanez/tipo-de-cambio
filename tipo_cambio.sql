
CREATE DATABASE  tipo_cambio_db;


-- ----------------------------
-- Sequence structure for moneda_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."moneda_id_seq";
CREATE SEQUENCE "public"."moneda_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for moneda_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."moneda_seq";
CREATE SEQUENCE "public"."moneda_seq" 
INCREMENT 50
MINVALUE  1
MAXVALUE 9223372036854775807
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for tipo_cambio_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."tipo_cambio_id_seq";
CREATE SEQUENCE "public"."tipo_cambio_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for transaccion_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."transaccion_id_seq";
CREATE SEQUENCE "public"."transaccion_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for usuario_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."usuario_id_seq";
CREATE SEQUENCE "public"."usuario_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for moneda
-- ----------------------------
DROP TABLE IF EXISTS "public"."moneda";
CREATE TABLE "public"."moneda" (
  "id" int8 NOT NULL DEFAULT nextval('moneda_id_seq'::regclass),
  "codigo" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "nombre" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Records of moneda
-- ----------------------------
INSERT INTO "public"."moneda" VALUES (1, 'PEN', 'SOLES');
INSERT INTO "public"."moneda" VALUES (2, 'USD', 'DOLARES');
INSERT INTO "public"."moneda" VALUES (3, 'EUR', 'EUROS');

-- ----------------------------
-- Table structure for tipo_cambio
-- ----------------------------
DROP TABLE IF EXISTS "public"."tipo_cambio";
CREATE TABLE "public"."tipo_cambio" (
  "id" int8 NOT NULL DEFAULT nextval('tipo_cambio_id_seq'::regclass),
  "moneda_origen_id" int8,
  "moneda_destino_id" int8,
  "valor" numeric(38,2) NOT NULL,
  "fecha" date NOT NULL
)
;

-- ----------------------------
-- Records of tipo_cambio
-- ----------------------------

-- ----------------------------
-- Table structure for transaccion
-- ----------------------------
DROP TABLE IF EXISTS "public"."transaccion";
CREATE TABLE "public"."transaccion" (
  "id" int8 NOT NULL DEFAULT nextval('transaccion_id_seq'::regclass),
  "monto" float8 NOT NULL,
  "monto_convertido" float8 NOT NULL,
  "tipo_cambio_id" int8,
  "usuario_id" int8,
  "fecha" timestamp(6) NOT NULL,
  "moneda_destino" varchar(255) COLLATE "pg_catalog"."default",
  "moneda_origen" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of transaccion
-- ----------------------------

-- ----------------------------
-- Table structure for usuario
-- ----------------------------
DROP TABLE IF EXISTS "public"."usuario";
CREATE TABLE "public"."usuario" (
  "id" int8 NOT NULL DEFAULT nextval('usuario_id_seq'::regclass),
  "nombre_usuario" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of usuario
-- ----------------------------
INSERT INTO "public"."usuario" VALUES (3, 'prsantibanez', '$2a$10$OjLh0IaqXKSmYT6mG.iAkuzTGHDsWchYZL7iXFi7uLqgXpOLs9jdu');
INSERT INTO "public"."usuario" VALUES (4, 'jrlopez', '$2a$10$ReUia8RHyKTopntYZH6j2.TPHjeEhxxSwDbqYsGzRYr5AkYu0lRuK');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."moneda_id_seq"
OWNED BY "public"."moneda"."id";
SELECT setval('"public"."moneda_id_seq"', 4, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
SELECT setval('"public"."moneda_seq"', 51, false);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."tipo_cambio_id_seq"
OWNED BY "public"."tipo_cambio"."id";
SELECT setval('"public"."tipo_cambio_id_seq"', 14, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."transaccion_id_seq"
OWNED BY "public"."transaccion"."id";
SELECT setval('"public"."transaccion_id_seq"', 5, true);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."usuario_id_seq"
OWNED BY "public"."usuario"."id";
SELECT setval('"public"."usuario_id_seq"', 5, true);

-- ----------------------------
-- Uniques structure for table moneda
-- ----------------------------
ALTER TABLE "public"."moneda" ADD CONSTRAINT "moneda_codigo_key" UNIQUE ("codigo");

-- ----------------------------
-- Primary Key structure for table moneda
-- ----------------------------
ALTER TABLE "public"."moneda" ADD CONSTRAINT "moneda_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tipo_cambio
-- ----------------------------
ALTER TABLE "public"."tipo_cambio" ADD CONSTRAINT "tipo_cambio_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table transaccion
-- ----------------------------
ALTER TABLE "public"."transaccion" ADD CONSTRAINT "transaccion_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table usuario
-- ----------------------------
ALTER TABLE "public"."usuario" ADD CONSTRAINT "usuario_nombre_usuario_key" UNIQUE ("nombre_usuario");

-- ----------------------------
-- Primary Key structure for table usuario
-- ----------------------------
ALTER TABLE "public"."usuario" ADD CONSTRAINT "usuario_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Keys structure for table tipo_cambio
-- ----------------------------
ALTER TABLE "public"."tipo_cambio" ADD CONSTRAINT "tipo_cambio_moneda_destino_id_fkey" FOREIGN KEY ("moneda_destino_id") REFERENCES "public"."moneda" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."tipo_cambio" ADD CONSTRAINT "tipo_cambio_moneda_origen_id_fkey" FOREIGN KEY ("moneda_origen_id") REFERENCES "public"."moneda" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Keys structure for table transaccion
-- ----------------------------
ALTER TABLE "public"."transaccion" ADD CONSTRAINT "transaccion_tipo_cambio_id_fkey" FOREIGN KEY ("tipo_cambio_id") REFERENCES "public"."tipo_cambio" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."transaccion" ADD CONSTRAINT "transaccion_usuario_id_fkey" FOREIGN KEY ("usuario_id") REFERENCES "public"."usuario" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
