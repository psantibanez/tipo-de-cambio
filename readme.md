# Tipo Cambio - Proyecto Spring Boot

Este proyecto se basa en Spring Boot y se centra en proporcionar una plataforma para realizar conversiones de tipo de cambio entre diferentes monedas. Además, ofrece un mantenedor para gestionar los tipos de cambio y registra el nombre del usuario que realiza las operaciones, información que se obtiene a través de un token Bearer.

## Requisitos Previos
- Tener instalado Docker y Docker Compose en su máquina.
- Java 17 para construir el proyecto (en caso de que desee construirlo manualmente).
- Maven para gestionar las dependencias y construir el proyecto (en caso de que desee construirlo manualmente).

## Configuración de la Base de Datos
El proyecto utiliza una base de datos PostgreSQL versión 13. Asegúrese de tener configuradas las variables de entorno necesarias en el archivo `.env` o en su entorno de ejecución.

## Levantando el Proyecto

1. Sitúese en la raíz del proyecto donde se encuentra el archivo `docker-compose.yml`.
2. Ejecute el siguiente comando para construir y levantar los servicios definidos en el archivo `docker-compose.yml`:

```bash
docker-compose up --build
```

## Acceso a la Aplicación y Swagger UI

Una vez que los servicios estén en ejecución, puede acceder a la aplicación a través de la siguiente URL:

- Aplicación: `http://localhost:8080/`
- Swagger UI para explorar y probar los endpoints de la API: `http://localhost:8080/swagger-ui/index.html#/`

## Desarrollo y Modificación

Para realizar modificaciones o desarrollar nuevas funcionalidades, asegúrese de tener un entorno de desarrollo adecuado con Java 17 y Maven configurados. Luego de realizar las modificaciones, puede construir el proyecto nuevamente y levantar los servicios usando Docker Compose como se describió anteriormente.

## Características Adicionales

El proyecto está configurado para leer las variables de entorno desde un archivo `.env` en la raíz del proyecto. Asegúrese de tener este archivo con las configuraciones necesarias antes de levantar los servicios.

## Contacto y Soporte

Para cualquier consulta o soporte adicional, por favor, contacte a los administradores del proyecto.

---

Este README proporciona una visión general básica para poner en marcha el proyecto y acceder a sus funcionalidades. Para una documentación más detallada, por favor, refiérase a la documentación del código fuente y los comentarios incluidos en el mismo.