package com.tipo.cambio.repository;

import com.tipo.cambio.model.Moneda;
import com.tipo.cambio.model.TipoCambio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoCambioRepository extends JpaRepository<TipoCambio, Long> {
     TipoCambio findByMonedaOrigenAndMonedaDestino(Moneda monedaOrigen, Moneda monedaDestino);

}
