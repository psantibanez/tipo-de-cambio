package com.tipo.cambio.repository;

import com.tipo.cambio.model.Transaccion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface TransaccionRepository extends JpaRepository<Transaccion, Long> {

}
