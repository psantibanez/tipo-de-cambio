package com.tipo.cambio.repository;

import com.tipo.cambio.model.Moneda;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonedaRepository extends JpaRepository<Moneda, Long> {
    Moneda findByCodigo(String codigo);

}
