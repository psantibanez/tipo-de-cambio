package com.tipo.cambio.controller;

import com.tipo.cambio.dto.ApiResponse;
import com.tipo.cambio.dto.TipoCambioDto;
import com.tipo.cambio.dto.TipoCambioRequestDto;
import com.tipo.cambio.dto.TransaccionDto;
import com.tipo.cambio.services.TipoCambioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RequestMapping("/api/v1/tipo-cambios")
@RestController
public class TipoCambioController {
    @Autowired
    private TipoCambioService tipoCambioService;

    @PostMapping()
    public Mono<ApiResponse<TipoCambioDto>> registrarTipoDeCambio(@RequestBody TipoCambioRequestDto tipoCambioRequestDto) {
        return tipoCambioService.registrarTipoDeCambio(tipoCambioRequestDto);
    }

    @PutMapping("/{id}")
    public Mono<ApiResponse<TipoCambioDto>> actualizarTipoDeCambio(@PathVariable Long id, @RequestBody TipoCambioRequestDto tipoDeCambioDTO) {
        return tipoCambioService.actualizarTipoDeCambio(id, tipoDeCambioDTO);
    }

    @GetMapping("/buscar")
    public Mono<ApiResponse<TipoCambioDto>> buscarTipoDeCambio(@RequestParam String monedaOrigenCodigo, @RequestParam String monedaDestinoCodigo) {
        return tipoCambioService.buscarTipoDeCambio(monedaOrigenCodigo, monedaDestinoCodigo);
    }

    @GetMapping()
    public Flux<TipoCambioDto> listarTiposDeCambio() {
        return tipoCambioService.listarTiposDeCambio();
    }

    @PostMapping("/realizar-tipo-cambio")
    public Mono<ResponseEntity<ApiResponse<TransaccionDto>>> realizarTipoDeCambio(@RequestBody TipoCambioRequestDto tipoCambioRequestDto) {
        return tipoCambioService.realizarTipoDeCambio(tipoCambioRequestDto)
                .map(response -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response));
    }
}
