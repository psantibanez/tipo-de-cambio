package com.tipo.cambio.controller;

import com.tipo.cambio.dto.ApiResponse;
import com.tipo.cambio.dto.LoginRequestDto;
import com.tipo.cambio.dto.LoginResponseDto;
import com.tipo.cambio.dto.RegistroUsuarioDto;
import com.tipo.cambio.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RequestMapping("/api/v1/usuarios")
@RestController
public class UsuarioController {
    @Autowired
    private UsuarioService usuarioService;
    @PostMapping("/registro")
    public Mono<ResponseEntity<ApiResponse<String>>> registrarUsuario(@RequestBody RegistroUsuarioDto registroUsuarioDto) {
        return usuarioService.registrarUsuario(registroUsuarioDto)
                .map(response -> ResponseEntity.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(response));
    }

    @PostMapping("/login")
    public Mono<ApiResponse<LoginResponseDto>> loginUsuario(@RequestBody LoginRequestDto registroUsuarioDto) {
        return usuarioService.authenticate(registroUsuarioDto);
    }
}
