package com.tipo.cambio.mappers;

import com.tipo.cambio.dto.TipoCambioDto;
import com.tipo.cambio.dto.TipoCambioRequestDto;
import com.tipo.cambio.dto.TransaccionDto;
import com.tipo.cambio.model.TipoCambio;
import com.tipo.cambio.model.Transaccion;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TipoDeCambioMapper {

    TipoDeCambioMapper INSTANCE = Mappers.getMapper(TipoDeCambioMapper.class);

    @Mapping(source = "monedaOrigen.codigo", target = "monedaOrigen")
    @Mapping(source = "monedaDestino.codigo", target = "monedaDestino")
    TipoCambioDto toDTO(TipoCambio tipoDeCambio);

    @Mapping(source = "monedaOrigen", target = "monedaOrigen.codigo")
    @Mapping(source = "monedaDestino", target = "monedaDestino.codigo")
    TipoCambio toEntity(TipoCambioRequestDto tipoDeCambioDTO);



    @Mapping(source = "tipoCambio.valor", target = "tipoCambioValor")
    @Mapping(source = "usuario.id", target = "usuarioId")
    @Mapping(source = "usuario.nombreUsuario", target = "nombreUsuario")
    TransaccionDto toDTOTransacciontDto(Transaccion transaccion);
}