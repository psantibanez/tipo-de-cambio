package com.tipo.cambio.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;


public class TipoCambioRequestDto {

    private String monedaOrigen;
    private String monedaDestino;
    private BigDecimal valor;
    private LocalDate fecha;

    public TipoCambioRequestDto() {
    }



    public String getMonedaOrigen() {
        return monedaOrigen;
    }

    public void setMonedaOrigen(String monedaOrigen) {
        this.monedaOrigen = monedaOrigen;
    }

    public String getMonedaDestino() {
        return monedaDestino;
    }

    public void setMonedaDestino(String monedaDestino) {
        this.monedaDestino = monedaDestino;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
}
