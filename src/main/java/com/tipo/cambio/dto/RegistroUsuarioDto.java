package com.tipo.cambio.dto;

import lombok.Data;

@Data
public class RegistroUsuarioDto {
    private String username;
    private String password;
}
