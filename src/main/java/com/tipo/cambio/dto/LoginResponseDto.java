package com.tipo.cambio.dto;

import lombok.Data;

@Data
public class LoginResponseDto {
    private String token;
}
