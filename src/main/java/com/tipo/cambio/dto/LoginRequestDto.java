package com.tipo.cambio.dto;

import lombok.Data;

@Data
public class LoginRequestDto {
    private String username;
    private String password;
}
