package com.tipo.cambio.services.impl;

import com.tipo.cambio.configuration.JwtConfig;
import com.tipo.cambio.dto.ApiResponse;
import com.tipo.cambio.dto.TipoCambioDto;
import com.tipo.cambio.dto.TipoCambioRequestDto;
import com.tipo.cambio.dto.TransaccionDto;
import com.tipo.cambio.mappers.TipoDeCambioMapper;
import com.tipo.cambio.model.Moneda;
import com.tipo.cambio.model.TipoCambio;
import com.tipo.cambio.model.Transaccion;
import com.tipo.cambio.model.Usuario;
import com.tipo.cambio.repository.MonedaRepository;
import com.tipo.cambio.repository.TipoCambioRepository;
import com.tipo.cambio.repository.TransaccionRepository;
import com.tipo.cambio.services.TipoCambioService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;

@Slf4j
@RequiredArgsConstructor
@Service
public class TipoCambioImpl  implements TipoCambioService {
    @Autowired
    private TipoCambioRepository tipoCambioRepository;
    @Autowired
    private MonedaRepository monedaRepository;

    @Autowired
    private TransaccionRepository transaccionRepository;
    @Autowired
    private JwtConfig jwtConfig;
    @Override
    public Mono<ApiResponse<TipoCambioDto>> registrarTipoDeCambio(TipoCambioRequestDto tipoCambioRequestDto) {
        return Mono.justOrEmpty(monedaRepository.findByCodigo(tipoCambioRequestDto.getMonedaOrigen()))
                 .flatMap(monedaOrigen -> Mono.justOrEmpty(monedaRepository.findByCodigo(tipoCambioRequestDto.getMonedaDestino()))
                          .flatMap(monedaDestino -> {
                             TipoCambio tipoDeCambio = TipoDeCambioMapper.INSTANCE.toEntity(tipoCambioRequestDto);
                             tipoDeCambio.setMonedaOrigen(monedaOrigen);
                             tipoDeCambio.setMonedaDestino(monedaDestino);
                             return Mono.justOrEmpty(tipoCambioRepository.save(tipoDeCambio))
                                      .map(savedTipoDeCambio -> new ApiResponse<>(1, "Registro exitoso", TipoDeCambioMapper.INSTANCE.toDTO(savedTipoDeCambio)))
                                     .onErrorResume(e -> Mono.just(new ApiResponse<>(0, e.getMessage(), null)));
                         }))
                .defaultIfEmpty(new ApiResponse<>(0, "Moneda no existe", null));
 }

    @Override
    public Mono<ApiResponse<TipoCambioDto>> actualizarTipoDeCambio(Long id, TipoCambioRequestDto tipoCambioRequestDto) {
        return findMonedaByCodigo(tipoCambioRequestDto.getMonedaOrigen())
                .flatMap(monedaOrigen -> findMonedaByCodigo(tipoCambioRequestDto.getMonedaDestino())
                        .flatMap(monedaDestino -> updateTipoDeCambio(id, tipoCambioRequestDto, monedaOrigen, monedaDestino)))
                .onErrorResume(e -> Mono.just(new ApiResponse<>(0, e.getMessage(), null)));
    }

    private Mono<Moneda> findMonedaByCodigo(String codigo) {
        return Mono.justOrEmpty(monedaRepository.findByCodigo(codigo))
                .switchIfEmpty(Mono.error(new Exception("Moneda con código " + codigo + " no encontrada")));
    }

    private Mono<ApiResponse<TipoCambioDto>> updateTipoDeCambio(Long id, TipoCambioRequestDto tipoCambioRequestDto, Moneda monedaOrigen, Moneda monedaDestino) {
        return Mono.justOrEmpty(tipoCambioRepository.findById(id))
                .switchIfEmpty(Mono.error(new Exception("Tipo de cambio no encontrado")))
                .flatMap(tipoDeCambio -> {
                    tipoDeCambio.setValor(tipoCambioRequestDto.getValor());
                    tipoDeCambio.setMonedaOrigen(monedaOrigen);
                    tipoDeCambio.setMonedaDestino(monedaDestino);
                    return Mono.just(tipoCambioRepository.save(tipoDeCambio))
                            .map(updatedTipoDeCambio -> new ApiResponse<>(1, "Actualización exitosa", TipoDeCambioMapper.INSTANCE.toDTO(updatedTipoDeCambio)));
                });
    }

    @Override
    public Mono<ApiResponse<TipoCambioDto>> buscarTipoDeCambio(String monedaOrigenCode, String monedaDestinoCode) {
        return Mono.fromCallable(() -> monedaRepository.findByCodigo(monedaOrigenCode))
                .flatMap(monedaOrigen -> Mono.fromCallable(() -> monedaRepository.findByCodigo(monedaDestinoCode))
                        .flatMap(monedaDestino -> Mono.fromCallable(() -> tipoCambioRepository.findByMonedaOrigenAndMonedaDestino(monedaOrigen, monedaDestino))
                                .map(tipoDeCambio -> new ApiResponse<>(1, "Búsqueda exitosa", TipoDeCambioMapper.INSTANCE.toDTO(tipoDeCambio)))))
                .defaultIfEmpty(new ApiResponse<>(0, "Tipo de cambio no encontrado, no registrado", null));
 }

    @Override
    public Flux<TipoCambioDto> listarTiposDeCambio() {
        return Flux.fromIterable(tipoCambioRepository.findAll())
                .map(TipoDeCambioMapper.INSTANCE::toDTO)
                .defaultIfEmpty(new TipoCambioDto());
    }


    @Override
    public Mono<ApiResponse<TransaccionDto>> realizarTipoDeCambio(TipoCambioRequestDto tipoCambioRequestDto) {
        return Mono.just(SecurityContextHolder.getContext())
                .flatMap(securityContext -> {
                    Usuario username = (Usuario) securityContext.getAuthentication().getPrincipal();
                    return procesarTransaccion(tipoCambioRequestDto, username);
                })
                .onErrorResume(e -> Mono.just(new ApiResponse<>(0, "Revisa si el token es el correcto" , null)));
    }

    private Mono<ApiResponse<TransaccionDto>> procesarTransaccion(TipoCambioRequestDto tipoCambioRequestDto, Usuario username) {
        return findMonedaByCodigo(tipoCambioRequestDto.getMonedaOrigen())
                .flatMap(monedaOrigen -> findMonedaByCodigo(tipoCambioRequestDto.getMonedaDestino())
                        .flatMap(monedaDestino -> obtenerTipoDeCambio(monedaOrigen, monedaDestino)
                                .flatMap(tipoDeCambio -> realizarCalculoYGuardarTransaccion(tipoCambioRequestDto, monedaOrigen, monedaDestino, tipoDeCambio, username))));
    }

    private Mono<TipoCambio> obtenerTipoDeCambio(Moneda monedaOrigen, Moneda monedaDestino) {
        return Mono.justOrEmpty(tipoCambioRepository.findByMonedaOrigenAndMonedaDestino(monedaOrigen, monedaDestino))
                .switchIfEmpty(Mono.error(new Exception("Tipo de cambio no registrado entre las monedas seleccionadas")));
    }

    private Mono<ApiResponse<TransaccionDto>> realizarCalculoYGuardarTransaccion(
            TipoCambioRequestDto tipoCambioRequestDto, Moneda monedaOrigen, Moneda monedaDestino,
            TipoCambio tipoDeCambio, Usuario username) {

        BigDecimal montoConvertido = tipoCambioRequestDto.getValor().multiply(tipoDeCambio.getValor());

        Transaccion transaccion = new Transaccion();
        transaccion.setMonedaOrigen(monedaOrigen.getCodigo());
        transaccion.setMonedaDestino(monedaDestino.getCodigo());
        transaccion.setMonto(tipoCambioRequestDto.getValor().doubleValue());
        transaccion.setMontoConvertido(montoConvertido.doubleValue());
        transaccion.setTipoCambio(tipoDeCambio);
        transaccion.setUsuario(username);

        return Mono.just(transaccionRepository.save(transaccion))
                .map(savedTransaccion -> new ApiResponse<>(1, "Transacción exitosa", TipoDeCambioMapper.INSTANCE.toDTOTransacciontDto(savedTransaccion)));
    }
}
