package com.tipo.cambio.services.impl;

import com.tipo.cambio.configuration.JwtConfig;
import com.tipo.cambio.dto.ApiResponse;
import com.tipo.cambio.dto.LoginRequestDto;
import com.tipo.cambio.dto.LoginResponseDto;
import com.tipo.cambio.dto.RegistroUsuarioDto;
import com.tipo.cambio.model.Usuario;
import com.tipo.cambio.repository.UserRepository;
import com.tipo.cambio.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class UsuarioServiceImpl implements UsuarioService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;


    @Autowired
    private ReactiveAuthenticationManager  authenticationManager;

    @Autowired
    private JwtConfig jwtConfig;

    @Override
    public Mono<ApiResponse<String>> registrarUsuario(RegistroUsuarioDto registroUsuarioDto) {
        return Mono.justOrEmpty(userRepository.findByNombreUsuario(registroUsuarioDto.getUsername()))
                .defaultIfEmpty(new Usuario())
                .flatMap(existingUser -> {
                    if (existingUser.getId() != null) {
                        return Mono.just(new ApiResponse<>(0, "El nombre de usuario ya existe.", null));
                    } else {
                        Usuario usuario = new Usuario();
                        usuario.setNombreUsuario(registroUsuarioDto.getUsername());
                        usuario.setPassword(passwordEncoder.encode(registroUsuarioDto.getPassword()));
                        return Mono.justOrEmpty(userRepository.save(usuario))
                                .map(savedUser -> new ApiResponse<>(1, "Usuario registrado con éxito", savedUser.getNombreUsuario()));
                    }
                })
               ;
    }


    public Mono<ApiResponse<LoginResponseDto>> authenticate(LoginRequestDto loginRequest) {
        UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                loginRequest.getUsername(), loginRequest.getPassword());

        return this.authenticationManager.authenticate(authToken)
                .flatMap(authentication -> {
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    return Mono.just(userRepository.findByNombreUsuario(loginRequest.getUsername()))
                            .flatMap(usuario -> {
                                String jwt = jwtConfig.generateToken(usuario);
                                LoginResponseDto responseDto = new LoginResponseDto();
                                responseDto.setToken(jwt);
                                return Mono.just(new ApiResponse<>(1, "Autenticación exitosa", responseDto));
                            });
                })
                .onErrorResume(AuthenticationException.class, e ->
                        Mono.just(new ApiResponse<>(0, "Credenciales inválidas", new LoginResponseDto()))
                )
                .onErrorResume(e -> Mono.just(new ApiResponse<>(0, e.getMessage(), null)));
    }


    @Override
    public Mono<UserDetails> findByUsername(String username) {
        return Mono.just(userRepository.findByNombreUsuario(username))
                .map(user -> User.withUsername(user.getNombreUsuario())
                        .password(user.getPassword())
                        .roles("USER")
                        .build())
                .switchIfEmpty(Mono.error(new UsernameNotFoundException("User not found")));
    }

}
