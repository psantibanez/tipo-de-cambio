package com.tipo.cambio.services;


import com.tipo.cambio.dto.ApiResponse;
import com.tipo.cambio.dto.LoginRequestDto;
import com.tipo.cambio.dto.LoginResponseDto;
import com.tipo.cambio.dto.RegistroUsuarioDto;
import org.springframework.security.core.userdetails.UserDetails;
import reactor.core.publisher.Mono;

public interface UsuarioService {
    Mono<ApiResponse<String>> registrarUsuario(RegistroUsuarioDto registroUsuarioDto);
    Mono<ApiResponse<LoginResponseDto>> authenticate(LoginRequestDto loginRequest);
    Mono<UserDetails> findByUsername(String username);
}
