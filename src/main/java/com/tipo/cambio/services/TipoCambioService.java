package com.tipo.cambio.services;

import com.tipo.cambio.dto.ApiResponse;
import com.tipo.cambio.dto.TipoCambioDto;
import com.tipo.cambio.dto.TipoCambioRequestDto;
import com.tipo.cambio.dto.TransaccionDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface TipoCambioService {
    Mono<ApiResponse<TipoCambioDto>> registrarTipoDeCambio(TipoCambioRequestDto tipoCambioRequestDto);
    Mono<ApiResponse<TipoCambioDto>> actualizarTipoDeCambio(Long id, TipoCambioRequestDto tipoCambioRequestDto);
    Mono<ApiResponse<TipoCambioDto>> buscarTipoDeCambio(String monedaOrigen, String monedaDestino);
    Flux<TipoCambioDto> listarTiposDeCambio();

    Mono<ApiResponse<TransaccionDto>> realizarTipoDeCambio(TipoCambioRequestDto tipoCambioRequestDto);
}
